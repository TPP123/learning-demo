package me.seu.demo.test;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author liangfeihu
 * @since 2020/4/15 10:54
 */
@Slf4j
public class DateFormatTest {

    public static final String SPLIT_STR = "_$_#_";

    public static void main(String[] args) {
        String fileName = "栈略%s拒付解约清单";
        String dateStr = DateFormatUtils.format(new Date(System.currentTimeMillis()), "yyyyMMdd");
        System.out.println(String.format(fileName, dateStr));

        String xml = "2_11_23";
        System.out.println(xml.contains("4"));

        List<Integer> one = Arrays.asList(1, 22, 3);
        System.out.println(one.contains(2));
        System.out.println(one.contains(22));
        System.out.println(one.contains("2"));
        System.out.println(one.contains("22"));

        DateTime dateTime = new DateTime(System.currentTimeMillis());
        String s = dateTime.plusYears(5).toString("yyyy-MM-dd");
        System.out.println(s);

        DateTime dateTime2 = new DateTime();
        String mMdd = dateTime2.toString("MMdd");
        System.out.println(mMdd);

        System.out.println("---------------------------");

        Date nowDate = new Date();
        Date startDate = DateUtils.truncate(nowDate, Calendar.DAY_OF_MONTH);
        Date midDate = DateUtils.setHours(startDate, 12);
        Date valDate = DateUtils.setHours(startDate, 20);

        Date begin = null;
        Date end = null;
        if (nowDate.getTime() > valDate.getTime()) {
            begin = midDate;
            end = nowDate;
        } else {
            begin = startDate;
            end = midDate;
        }
        log.info("[summaryClaimReturn] begin={} end={}", DateFormatUtils.format(begin, "yyyy-MM-dd HH:mm:ss"),
                DateFormatUtils.format(end, "yyyy-MM-dd HH:mm:ss"));

        System.out.println("---------------------------");

        String xmlStr = "123456" + SPLIT_STR + "sdhaknjlklklk";
        int i = xmlStr.indexOf(SPLIT_STR);
        System.out.println(xmlStr.substring(0, i));
        System.out.println(xmlStr.substring(i + 5));


        System.out.println(xmlStr.substring(i + SPLIT_STR.length()));

        Date date = new Date();
        Date handleDate = DateUtils.addDays(date, -14);
        log.info("[ClaimZipClean] begin={} end={}", DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss"),
                DateFormatUtils.format(handleDate, "yyyy-MM-dd HH:mm:ss"));
    }
}

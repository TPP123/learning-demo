package me.seu.demo.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 垫付服务同步给外部系统返回值
 *
 * @author liangfeihu
 * @since 2020/6/16 11:41
 */
@Data
@NoArgsConstructor
@ApiModel(description = "垫付服务同步给外部系统返回值")
public class PaymentAdvanceInfoSyncResponse {

    @ApiModelProperty(value = "应答码")
    private String code;

    @ApiModelProperty(value = "应答信息")
    private String message;

    @ApiModelProperty(value = "返回状态")
    private String state;

}


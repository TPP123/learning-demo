package me.seu.demo.test.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

/**
 * 读取配置文件
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/12 下午4:49
 */
public class PropertyLoadTest {

    private static final String path = "/Users/a123/.app_config/biz-iot-lfh-demo-srv/dev/config.props";

    public static void main(String[] args) throws Exception {
        File file = new File(path);

        Properties props = new Properties();
        loadProperties(props, file);
        System.out.println(props.toString());

        System.out.println("---------------------------");

        Properties props2 = new Properties();
        loadPropertiesWithUtf8(props2, file);
        System.out.println(props2.toString());

        System.out.println(unixStyleToJavaStyle("min-Idle"));
        System.out.println(unixStyleToJavaStyle("maxActive"));
        System.out.println(unixStyleToJavaStyle("initial-size"));

    }

    public static void loadPropertiesWithUtf8(Properties props, File file) throws Exception {
        InputStreamReader ipr = null;

        try {
            ipr = new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8"));
            props.load(ipr);
        } catch (IOException var11) {
            throw new RuntimeException(var11.getMessage(), var11);
        } finally {
            if (ipr != null) {
                try {
                    ipr.close();
                } catch (Throwable var10) {
                }
            }
        }

    }

    public static void loadProperties(Properties props, File file) {
        FileInputStream fin = null;

        try {
            fin = new FileInputStream(file);
            props.load(fin);
        } catch (IOException var11) {
            throw new RuntimeException(var11.getMessage(), var11);
        } finally {
            if (fin != null) {
                try {
                    fin.close();
                } catch (Throwable var10) {
                }
            }

        }

    }

    /**
     * 转化格式：initial-size  -> initialSize
     *
     * @param propName
     * @return
     */
    public static String unixStyleToJavaStyle(String propName) {
        StringBuffer javaStyle = new StringBuffer();
        StringBuffer unixStyle = new StringBuffer(propName);
        int fromIndex = 0;

        int idx;
        do {
            idx = unixStyle.indexOf("-", fromIndex);
            int toIndex;
            if (idx < 0) {
                toIndex = unixStyle.length();
            } else {
                toIndex = idx;
            }

            String word = unixStyle.substring(fromIndex, toIndex);
            if (fromIndex > 0) {
                word = capitalize(word);
            }

            javaStyle.append(word);
            fromIndex = idx + 1;
        } while (idx >= 0);

        return javaStyle.toString();
    }

    public static String capitalize(String str) {
        if (str.length() == 0) {
            return str;
        } else {
            StringBuffer sb = new StringBuffer();
            sb.append(Character.toUpperCase(str.charAt(0)));
            sb.append(str.substring(1));
            return sb.toString();
        }
    }

}

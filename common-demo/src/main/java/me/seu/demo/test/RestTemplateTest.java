package me.seu.demo.test;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author liangfeihu
 * @since 2020/6/17 17:49
 */
public class RestTemplateTest {

    public static void main(String[] args) {

        String requestUrl = "http://183.60.22.143/sns/ics-zlsb-service-sit.do?access_token=80169ba1e9ec4a1b83dfcdf2451ccf92&open_id=72a19b6bf67b4f3e9bacc79b1579c1e5";

        RestTemplate restTemplate = new RestTemplate();

        String requestBody = "{\"data\":{\"reqBizData\":{\"accidentDate\":1591261298404,\"insuredIdNo\":\"462783199103152822\",\"insuredIdType\":\"居民身份证\",\"insuredName\":\"胡送\",\"paymentAdvanceList\":[{\"paymentAdvanceAmount\":1.0,\"paymentAdvanceTime\":\"2020-06-17 17:45:04\",\"serialNo\":14}],\"policyId\":\"8G2013033202000000000294\",\"registNo\":\"62120130332020N0000003035\",\"sumNum\":1,\"sumPaymentAdvanceAmount\":1.0}},\"interfaceCode\":\"paymentAdvanceFromZHLToNclm\",\"requestSource\":\"O-ZHL\",\"requestTime\":1592450180853} ";

        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> httpEntity = new HttpEntity<String>(requestBody, headers);

        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(requestUrl, httpEntity, String.class);
        System.out.println(stringResponseEntity.getBody());
    }

}

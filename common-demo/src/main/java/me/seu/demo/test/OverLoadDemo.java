package me.seu.demo.test;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * test sl4j
 *
 * @author liangfeihu
 * @since 2020/1/11 13:00
 */
@Slf4j
public class OverLoadDemo {
    public static void invoke(String s, int i, Object... args) {
        System.out.println("无装拆箱" + i);
    }

    public static void invoke(Integer s, Object obj, Object... args) {
        System.out.println("object类型参数" + obj);
    }

    public static void main(String[] args) {
        int i = 2;
        //invoke("hello", i);
        //invoke(null, i);
        //System.out.println("00003678236767".startsWith("000036"));

        log.info("1={} 2={} 3={} 4={}", "hello", "LFH", "and", "Linux!");

        String idNo = "1038119930825785x";
        log.info("original id={}, update id = {}", idNo, idNo.toUpperCase());

        Date endDate = new Date();
        Date truncate = DateUtils.truncate(endDate, Calendar.DAY_OF_MONTH);
        Date beginDate = DateUtils.addDays(truncate, -1);
        log.info("endDate = {}", DateFormatUtils.format(endDate, "yyyy-MM-dd HH:mm:ss"));
        log.info("truncate = {}", DateFormatUtils.format(truncate, "yyyy-MM-dd HH:mm:ss"));
        log.info("beginDate = {}", DateFormatUtils.format(beginDate, "yyyy-MM-dd HH:mm:ss"));

        Date setHours = DateUtils.setHours(truncate, 12);
        log.info("setHours = {}", DateFormatUtils.format(setHours, "yyyyMMdd HH:mm:ss"));

        Date setDays = DateUtils.setDays(beginDate, 20);
        log.info("setDays = {}", DateFormatUtils.format(setDays, "yyyyMMdd HH:mm:ss"));

        Date setYears = DateUtils.setYears(beginDate, 2010);
        log.info("setYears = {}", DateFormatUtils.format(setYears, "yyyyMMdd HH:mm:ss"));

        log.info("[summaryClaimReturn] [{} --- {}]", DateFormatUtils.format(beginDate, "yyyy-MM-dd HH:mm:ss"),
                DateFormatUtils.format(endDate, "yyyy-MM-dd HH:mm:ss"));


        String rawPath = "/a/b/c/d/e";
        String[] pathArray = StringUtils.tokenizeToStringArray(rawPath, "/");
        System.out.println(JSONObject.toJSONString(pathArray));
        String collect = Arrays.stream(pathArray).skip(1L).collect(Collectors.joining("/"));
        System.out.println(collect);




    }


}

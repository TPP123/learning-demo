package me.seu.demo.test.hex;

import java.util.Arrays;

/**
 * 字节转化测试
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/3 上午12:20
 */
public class ByteTest {

    public static void main(String[] args) {
        byte len = (byte) 0x80;
        System.out.println(len);

        System.out.println(String.format("%02x", len));
        System.out.println(String.format("%02x", (byte)0x02));

        /*byte[] login = new byte[]{
                0x78, 0x78, 0x00, 0x11, 0x01,
                0x07, 0x52, 0x53, 0x36, 0x78, (byte) 0x90, 0x02, 0x42, // IMEI 号
                0x70, 0x00, 0x32, 0x01, 0x00, 0x05,
                0x12, 0x79, // CRC-ITU 值
                0x0D, 0x0A
        };
        byte[] crcBytes = Arrays.copyOfRange(login, 2, 19);
        System.out.println(ByteUtilTest.byteToHex(crcBytes, true));

        char crc16 = ItuUtil.getCrc16(crcBytes, crcBytes.length);
        byte one = (byte) ((crc16 >> 8) & 0xff);
        byte two = (byte) (crc16 & 0xff);
        String strOne = Integer.toHexString(one | 0xffffff00).toUpperCase();
        System.out.println(strOne.substring(6));

        String strTwo = Integer.toHexString(two | 0xffffff00).toUpperCase();
        System.out.println(strTwo.substring(6));*/

        // 78 78 05 23 01 00 67 0E 0D 0A
        byte[] reply = new byte[]{0x78, 0x78, 0x00, 0x05, 0x23, 0x01, 0x00, 0x67, 0x0E, 0x0D, 0x0A};
        byte[] reply2 = new byte[]{0x00, 0x05, 0x23, 0x01, 0x00};
        char crc2 = ItuUtil.getCrc16(reply2, reply2.length);
        byte one2 = (byte) ((crc2 >> 8) & 0xff);
        byte two2 = (byte) (crc2 & 0xff);
        String strOne2 = Integer.toHexString(one2 | 0xffffff00).toUpperCase();
        System.out.println(strOne2.substring(6));

        String strTwo2 = Integer.toHexString(two2 | 0xffffff00).toUpperCase();
        System.out.println(strTwo2.substring(6));

    }

}

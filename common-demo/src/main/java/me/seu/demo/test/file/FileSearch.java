package me.seu.demo.test.file;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author liangfeihu
 * @since 2020/12/29 下午6:41
 */
public class FileSearch {

    private static final String searchWord = "110106198807184513";
    private static final String directoryPath = "/Users/liangfeihu/Documents/work_project/bq/28";

    public static void main(String[] args) {
        //创建一个 File 实例，表示路径名是指定路径参数的文件
        File file = new File(directoryPath);
        File[] listFiles = file.listFiles();
        System.out.println("fileSize=" + listFiles.length);
        //得到一个File数组，它默认是按文件最后修改日期排序的
        for (int i = 0; i < listFiles.length; i++) {
            if (listFiles[i].isDirectory()){
                System.out.println(listFiles[i].getName() + " isDirectory ");
            } else {
                search(listFiles[i], searchWord);
            }
        }

    }

    public static boolean isTrueFile(File file) {
        if (!file.exists() || !file.canRead())
            return false;
        if (file.getName().startsWith("."))
            return false;
        if (file.getName().endsWith("."))
            return false;
        return true;
    }

    public static void search(File file, String word) {
        try (FileInputStream fileInputStream = new FileInputStream(file);
             InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {

            String lineStr = null;
            while ((lineStr = bufferedReader.readLine()) != null) {
                if (lineStr.contains(word)) {
                    System.out.println("-----------" + file.getName());
                    break;
                }
            } // end while

        } catch (Exception e) {
            System.out.println("--error--");
        }
    }

}

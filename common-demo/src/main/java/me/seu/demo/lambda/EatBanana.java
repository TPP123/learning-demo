package me.seu.demo.lambda;

/**
 * 吃香蕉
 *
 * @author liangfeihu
 * @since 2020/3/4 16:01
 */
public class EatBanana implements EatFruit {
    private final String fruitName = "Banana";

    /**
     * 吃香蕉
     */
    @Override
    public void eat() {
        System.out.println("People like to eat " + fruitName + " !");
    }
}

package me.seu.demo.lambda;

/**
 * 吃水果的人
 *
 * @author liangfeihu
 * @since 2020/3/4 16:04
 */
public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    private void loveEatOrange() {
        System.out.println(name + " also like to eat Orange !");
    }

    public EatFruit getPersonEatAction() {
        return this::loveEatOrange;
    }

}

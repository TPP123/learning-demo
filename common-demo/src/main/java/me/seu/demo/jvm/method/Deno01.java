package me.seu.demo.jvm.method;

import java.util.Random;

/**
 * @author liangfeihu
 * @since 2020/1/9 16:29
 */
public class Deno01 {
    public static void main(String[] args) {
        商户 demo = new 奸商();
        double price = demo.折后价格(100d, () -> {
            return true;
        });
        System.out.println(price);
    }
}

interface 客户 {
    boolean isVIP();
}


class 商户 {
    public double 折后价格(double 原价, 客户 某客户) {
        return 原价 * 0.8d;
    }
}

class 奸商 extends 商户 {

    @Override
    public double 折后价格(double 原价, 客户 某客户) {
        // invokeinterface
        if (某客户.isVIP()) {
            // invokestatic
            return 原价 * 价格歧视();
        } else {
            // invokespecial
            return super.折后价格(原价, 某客户);
        }
    }

    public static double 价格歧视() {
        // 咱们的杀熟算法太粗暴了，应该将客户城市作为随机数生成器的种子。
        // invokespecial
        return new Random()
                // invokevirtual
                .nextDouble()
                + 0.8d;
    }

}

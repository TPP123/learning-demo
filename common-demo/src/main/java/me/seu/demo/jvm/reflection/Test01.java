package me.seu.demo.jvm.reflection;

import java.lang.reflect.Method;

/**
 * @author liangfeihu
 * @since 2020/1/13 15:10
 */
public class Test01 {
    public static void target(int i) {
        // 空方法
    }


    public static void main(String[] args) throws Exception {
        Class<?> klass = Class.forName("me.seu.demo.me.seu.demo.jvm.reflection.Test01");
        Method method = klass.getMethod("target", int.class);

        // 关闭权限检查
        method.setAccessible(true);
        polluteProfile();

        long current = System.currentTimeMillis();
        for (int i = 1; i <= 2_000_000_000; i++) {
            if (i % 1_000_000_000 == 0) {
                long temp = System.currentTimeMillis();
                System.out.println(temp - current);
                current = temp;
            }

            method.invoke(null, 125);
        }
    }


    public static void polluteProfile() throws Exception {
        Method method1 = Test01.class.getMethod("target", int.class);
        Method method2 = Test01.class.getMethod("target", int.class);
        for (int i = 0; i < 2000; i++) {
            method1.invoke(null, 0);
            method2.invoke(null, 0);
        }
        System.out.println(method1 == method2);
        System.out.println(method1.equals(method2));
    }

    public static void target1(int i) {
    }

    public static void target2(int i) {
    }
}

package me.seu.demo.jvm;

/**
 * @author liangfeihu
 * @since 2020/1/8 18:14
 */
public class Foo {
    // me.seu.demo.test

    private static final int VALUE = 3;

    private final static long BIG_VALUE;
    static {
        BIG_VALUE = 33L;
    }

    static boolean boolValue;

    public static void main(String[] args) {

        // 将这个 true 替换为 2 或者 3，再看看打印结果
        boolValue = true;

        if (boolValue) {
            System.out.println("Hello, Java!");
        }

        if (boolValue == true) {
            System.out.println("Hello, JVM!");
        }

    }
}

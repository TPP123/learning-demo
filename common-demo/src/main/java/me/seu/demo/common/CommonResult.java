package me.seu.demo.common;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 通用返回对象
 *
 * @author liangfeihu
 * @since 2020/3/7 11:04
 */
@Data
@NoArgsConstructor
public class CommonResult implements Serializable {
    private static final long serialVersionUID = 1L;

    private int code;
    private String message;
    private Object data;

    protected CommonResult(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static CommonResult success(Object data) {
        return new CommonResult(200, "OK", data);
    }

    /**
     * 成功返回结果
     *
     * @param data    获取的数据
     * @param message 提示信息
     */
    public static CommonResult success(Object data, String message) {
        return new CommonResult(200, message, data);
    }

    /**
     * 失败返回结果
     *
     * @param errorCode 错误码
     */
    public static CommonResult failed(int errorCode) {
        return new CommonResult(500, "request failure", null);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static CommonResult failed(String message) {
        return new CommonResult(500, message, null);
    }

    /**
     * 失败返回结果
     */
    public static CommonResult failed() {
        return failed(400);
    }

    /**
     * 未登录返回结果
     */
    public static CommonResult unauthorized(Object data) {
        return new CommonResult(401, "请登录后操作", data);
    }

    /**
     * 未授权返回结果
     */
    public static CommonResult forbidden(Object data) {
        return new CommonResult(403, "禁止访问", data);
    }

    public static CommonResult failed(int code, String msg) {
        return new CommonResult(code, msg, null);
    }
}

package me.seu.demo.algorithms.unionfind;

/**
 * 并查集接口
 *
 * @author liangfeihu
 * @since 2021/2/22 下午6:19
 */
public interface UF {

    int getSize();

    boolean isConnected(int p, int q);

    void unionElements(int p, int q);

}

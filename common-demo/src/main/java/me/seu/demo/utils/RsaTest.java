package me.seu.demo.utils;

import org.apache.commons.codec.binary.Base64;
import org.springframework.cloud.bootstrap.encrypt.KeyProperties;
import org.springframework.core.io.FileSystemResource;
//import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * RsaTest
 * <p>
 * encrypt:
 *      key-store:
 *          location: classpath:zlt.jks
 *              secret: zlt!@#$
 *              alias: zlt
 * </p>
 * @author liangfeihu
 * @since 2020/12/30 下午3:58
 */
public class RsaTest {

    /**
     * 用于封装随机产生的公钥与私钥
     */
    private static Map<Integer, String> keyMap = new HashMap<Integer, String>();

    public static void main(String[] args) throws Exception{
        KeyProperties.KeyStore keyStore = new KeyProperties.KeyStore();

        FileSystemResource pathResource = new FileSystemResource("/Users/liangfeihu/projects/gitee/myown/learning-demo/common-demo/src/main/resources/rsa/zlt.jks");
        keyStore.setLocation(pathResource);
        keyStore.setAlias("zlt");
        keyStore.setSecret("zlt!@#$");

        KeyProperties keyProperties = new KeyProperties();
        keyProperties.setKeyStore(keyStore);

        //KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(pathResource, keyProperties.getKeyStore().getSecret().toCharArray());
        KeyPair keyPair = null;//keyStoreKeyFactory.getKeyPair(keyProperties.getKeyStore().getAlias());

        // 得到私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        // 得到私钥字符串
        String privateKeyString = new String(Base64.encodeBase64((privateKey.getEncoded())));

        // 得到公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        String publicKeyString = new String(Base64.encodeBase64(publicKey.getEncoded()));

        System.out.println("随机生成的公钥为:" + publicKeyString);
        System.out.println("---------------------------------------------------");
        System.out.println("随机生成的私钥为:" + privateKeyString);

        // 将公钥和私钥保存到Map：0表示公钥 1表示私钥
        keyMap.put(0, publicKeyString);
        keyMap.put(1, privateKeyString);

        System.out.println("------------------开始加密-----------------------");

        //加密字符串
        String message = "Hello RSA Algorithm !";

        String encrypt1 = RSAEncrypt.encrypt(message, publicKeyString);
        System.out.println("encrypt msg1=" + encrypt1);

        // RSA算法每次加密结果都不一样的，用配套的秘钥解密即可
        String encrypt11 = RSAEncrypt.encrypt(message, publicKeyString);
        System.out.println("encrypt msg11=" + encrypt11);

        String encrypt2 = RSAEncrypt.encryptByPrivateKey(message, privateKeyString);
        System.out.println("encrypt msg2=" + encrypt2);
        String encrypt3 = RsaUtil.encrypt(message, publicKey);
        System.out.println("encrypt msg3=" + encrypt3);

        String decrypt1 = RSAEncrypt.decrypt(encrypt1, privateKeyString);
        System.out.println("decrypt msg1=" + decrypt1);

        String decrypt2 = RSAEncrypt.decryptByPublicKey(encrypt2, publicKeyString);
        System.out.println("decrypt msg2=" + decrypt2);

        String decrypt3 = RsaUtil.decrypt(encrypt3, privateKey);
        System.out.println("decrypt msg3=" + decrypt3);

    }

}

package me.seu.demo.service.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * BaseResource
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/27 下午5:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResource {

    private String name;

}

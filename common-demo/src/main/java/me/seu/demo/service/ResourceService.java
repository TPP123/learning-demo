package me.seu.demo.service;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.bean.BaseResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用于实现策略模式
 * 注入 BaseResource 所有实现类到List、Map中
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/27 下午5:07
 */
@Slf4j
@Service
public class ResourceService {

    @Autowired
    private List<BaseResource> resourceList = new ArrayList<>(5);

    @Autowired
    private Map<String, BaseResource> resourceMap = new ConcurrentHashMap<>(5);

    @PostConstruct
    public void init() {
        for (BaseResource baseResource : resourceList) {
            log.info("[ResourceService] add resource api={}", baseResource.getName());
        }

        Set<Map.Entry<String, BaseResource>> entrySet = resourceMap.entrySet();
        for (Map.Entry<String, BaseResource> entry : entrySet) {
            log.info("[ResourceService] resource key={} value={} ", entry.getKey(), entry.getValue().getName());
        }
    }

}

package me.seu.demo.shardingsphere.hint;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Hint Service
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:09
 */
public interface HintService {

    public void processWithHintValueForShardingDatabases() throws SQLException, IOException;

    public void processWithHintValueForShardingDatabasesAndTables() throws SQLException, IOException;

    public void processWithHintValueMaster() throws SQLException, IOException;

}

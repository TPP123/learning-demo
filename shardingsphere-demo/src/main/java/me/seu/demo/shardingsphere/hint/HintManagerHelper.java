package me.seu.demo.shardingsphere.hint;

import org.apache.shardingsphere.api.hint.HintManager;

/**
 * HintManager Helper
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:08
 */
public class HintManagerHelper {

    public static void initializeHintManagerForShardingDatabasesAndTables(final HintManager hintManager) {
        // 只走库 ds3
        hintManager.addDatabaseShardingValue("health_record", 3L);
        // 只走表 health_record2
        hintManager.addTableShardingValue("health_record", 2L);
    }

    public static void initializeHintManagerForShardingDatabases(final HintManager hintManager) {
        // 只走库ds1
        hintManager.setDatabaseShardingValue(1L);
    }

    public static void initializeHintManagerForMaster(final HintManager hintManager) {
        // 只走主库
        hintManager.setMasterRouteOnly();
    }

}

package me.seu.demo.shardingsphere.repository;

import me.seu.demo.shardingsphere.entity.HealthTask;
import org.apache.ibatis.annotations.Mapper;

/**
 * 健康任务表 Dao
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:46
 */
@Mapper
public interface HealthTaskRepository extends BaseRepository<HealthTask, Long> {

}

package me.seu.demo.shardingsphere.service.impl;

import me.seu.demo.shardingsphere.entity.User;
import me.seu.demo.shardingsphere.repository.UserRepository;
import me.seu.demo.shardingsphere.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户 Service
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:07
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void processUsers() throws SQLException {
        insertUsers();
    }

    private List<Long> insertUsers() throws SQLException {
        List<Long> result = new ArrayList<>(10);
        for (Long i = 1L; i <= 10; i++) {
            User user = new User();
            user.setUserId(i);
            user.setUserName("user_" + i);
            userRepository.addEntity(user);
            result.add(user.getUserId());
            System.out.println("Insert User:" + user.getUserId());
        }
        return result;
    }

    @Override
    public List<User> getUsers() throws SQLException {
        List<User> userList = userRepository.findEntities();
        System.out.println(userList.toString());
        return userList;
    }

}

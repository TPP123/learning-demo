package me.seu.demo.shardingsphere.service.impl;

import me.seu.demo.shardingsphere.entity.EncryptUser;
import me.seu.demo.shardingsphere.repository.EncryptUserRepository;
import me.seu.demo.shardingsphere.service.EncryptUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 加密用户 Service
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:05
 */
@Service
public class EncryptUserServiceImpl implements EncryptUserService {

    @Autowired
    private EncryptUserRepository encryptUserRepository;

    @Override
    public void processEncryptUsers() throws SQLException {
        insertEncryptUsers();
    }

    private List<Long> insertEncryptUsers() throws SQLException {
        List<Long> result = new ArrayList<>(10);
        for (Long i = 1L; i <= 10; i++) {
            EncryptUser encryptUser = new EncryptUser();
            encryptUser.setUserId(i);
            encryptUser.setUserName("test_" + i);
            encryptUser.setPwd("pwd" + i);
            encryptUserRepository.addEntity(encryptUser);
            result.add(encryptUser.getUserId());
        }

        return result;
    }

    @Override
    public List<EncryptUser> getEncryptUsers() throws SQLException {
        List<EncryptUser> encryptUserList = encryptUserRepository.findEntities();
        System.out.println(encryptUserList.toString());
        return encryptUserList;
    }

}

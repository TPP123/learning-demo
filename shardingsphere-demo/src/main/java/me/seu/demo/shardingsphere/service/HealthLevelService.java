package me.seu.demo.shardingsphere.service;

import java.sql.SQLException;

/**
 * 健康级别 Service
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:02
 */
public interface HealthLevelService {

    public void processLevels() throws SQLException;

}

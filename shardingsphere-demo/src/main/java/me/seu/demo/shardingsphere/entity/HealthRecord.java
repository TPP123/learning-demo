package me.seu.demo.shardingsphere.entity;

/**
 * 健康记录表
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:30
 */
public class HealthRecord {

    Long recordId;
    Long userId;
    Long levelId;
    String remark;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}

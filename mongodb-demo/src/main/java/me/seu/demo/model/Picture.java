package me.seu.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Mongodb文档对象
 *
 * @author liangfeihu
 * @since 2020/4/27 18:58
 */
@Data
@NoArgsConstructor
@Document(collection = "picture")
public class Picture implements Serializable {

	/**
     * id供mongodb内部使用
     */
    @Id
    private String id;
    private String filename;
    private String path;
    private Long size;

    public Picture(String filename, String path, Long size) {
        this.filename = filename;
        this.path = path;
        this.size = size;
    }

    @Override
    public String toString() {
        return "Picture [id=" + id + ", filename=" + filename + ", path=" + path + ", size=" + size + "]";
    }

}

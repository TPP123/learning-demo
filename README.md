# learning-demo

#### 介绍
个人学习练习项目，涵盖了多线程、设计模式、JVM、全局异常管理;
Kafka、RocketMQ等MQ中间件;
MongoDB、ElasticSearch、Redis等NoSQL存储；
ShardingSphere分库分表、主从读写分离中间件实战应用。



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

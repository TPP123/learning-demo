package me.seu.demo.repository;

import me.seu.demo.entity.Item;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * Item es dao
 *
 * @author liangfeihu
 * @since 2020/4/22 18:28
 */
@Repository
public interface ItemRepository extends ElasticsearchRepository<Item, Long> {
}

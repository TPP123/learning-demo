package me.seu.demo.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

/**
 * orderly queue one 消费者
 *
 * @author liangfeihu
 * @since 2020/4/16 18:30
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "orderly-topic-one", consumerGroup = "my-consumer_orderly_topic_one", consumeMode = ConsumeMode.ORDERLY, consumeThreadMax = 4)
public class OrderlyOneConsumer implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt msgExt) {
        try {
            log.info("[{}] one queueId={} received orderly msg: {}", Thread.currentThread().getName(), msgExt.getQueueId(), new String(msgExt.getBody(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
